import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

# Connect to Chrome driver
browser = webdriver.Chrome(executable_path='./chromedriver.exe')
browser.maximize_window()

# Set credentials
login = input('Email: ')
pwd = input('Password: ')
title = input('Book title: ')
pages = input('Number of pages: ')

# Default timeout
wait = WebDriverWait(browser, 10)

# Connect to login page
browser.get('https://www.ibuk.pl/logowanie.html')

# Close annoying popup
wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button.close.close__m__btn')))
browser.find_element_by_css_selector('button.close.close__m__btn').click()

# Fill credentials fields
field = browser.find_element_by_id('loginform-username')
field.send_keys(login)
field = browser.find_element_by_id('loginform-password')
field.send_keys(pwd)

# Close another annoying popup
wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'a.cookies-wrapper__btn')))
browser.find_element_by_css_selector('a.cookies-wrapper__btn').click()

# Submit login form
print(f'Trying to login...')
wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button.main-button.main-button--blue.login__button.login__button--login')))
time.sleep(1.5)
browser.find_element_by_css_selector('button.main-button.main-button--blue.login__button.login__button--login').click()

# Go to IBUK bookshelf
wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'button.btn.btn-link.logout')))
print(f'Login successfully')
wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[title^='myIBUK']")))
browser.get('https://reader.ibuk.pl/')

# Find book to scan
print(f'Searching for {title}')
wait.until(EC.element_to_be_clickable((By.XPATH, f"//*[contains(text(), '{title}')]")))
browser.find_element_by_xpath(f"//*[contains(text(), '{title}')]").click()

wait.until(EC.visibility_of_element_located((By.ID, 'page')))
browser.find_element_by_id('page').click()

wait.until(EC.visibility_of_element_located((By.ID, 'go-to-page')))
field = browser.find_element_by_id('go-to-page')
field.send_keys('1')
field.send_keys(Keys.ENTER)
print(f'Start collecting {pages} of {title} book...')
time.sleep(10)

# Scan every page
for i in range(int(pages)):
    browser.save_screenshot(f'page{i}.png')
    browser.find_element_by_id('nextPageImg').click()
    time.sleep(2.0)
browser.quit()